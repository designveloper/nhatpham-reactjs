import React, { Component, PropTypes } from 'react';

//stateless component
const resorts = [
  "Logo Babe",
  "Yame KIKI",
  "Alo Alo",
  "Babel Babel",
  "Hot and Cold"
]
class Autocomplete extends Component {
  get value() {
    return this.refs.inputResort.value;
  }
  set value(inputValue) {
    this.refs.inputResort.value = inputValue;
  }
  render() {
    console.log(this.props.options);
    return (
      <div>
        <input ref="inputResort" type="text" list='resortss' className="form-control" />
        <datalist id="resortss">
          {
            this.props.options.map((opt, i) => {
             return <option key={i}>{opt}</option>
            })
          }
        </datalist>
      </div>
    )
  }
}

const AddDate = ({ resort, date, powder, backcountry, onNewDay }) => {
  let _resort, _date, _powder, _backcountry;
  const submitDay = (e) => {
    e.preventDefault();
    let NewDay = {
      resort: _resort.value,
      date: new Date(_date.value),
      powder: _powder.checked,
      backcountry: _backcountry.checked
    }
    console.log(NewDay);
    onNewDay(NewDay);
    // console.log(onNewDay);
  }
  return (
    <form className="add-day-form form" onSubmit={submitDay}>
      <div className="form-group">
        <label htmlFor="resort" className="col-sm-2 control-label">Resort</label>
        <div className="col-sm-10">
          <Autocomplete options={resorts} ref={input => _resort = input} />
        </div>
        <label htmlFor="date" className="col-sm-2 control-label">Date</label>
        <div className="col-sm-10">
          <input ref={input => _date = input} type="date" name="date" id="date" className="form-control" defaultValue={date} />
        </div>
        <label htmlFor="powder" className="col-sm-2 control-label">Powder</label>
        <div className="col-sm-10">
          <input ref={input => _powder = input} type="checkbox" name="powder" id="powder" defaultChecked={powder} />
        </div>
        <div className="clearfix"></div>
        <label htmlFor="backcountry" className="col-sm-2 control-label">Backcountry</label>
        <div className="col-sm-10">
          <input ref={input => _backcountry = input} type="checkbox" name="backcountry" id="backcountry" defaultChecked={backcountry} />
        </div>
        <div className="col-sm-10">
          <button type="submit" className="btn btn-primary pull-right">Add day</button>
        </div>
      </div>
    </form>
  )
}
AddDate.defaultProps = {
  resort: 'Halo',
  date: new Date(),
  powder: true,
  backcountry: true
}
AddDate.propTypes = {
  resort: PropTypes.string.isRequired,
  date: PropTypes.instanceOf(Date).isRequired,
  powder: PropTypes.bool.isRequired,
  backcountry: PropTypes.bool.isRequired
}
export default AddDate;