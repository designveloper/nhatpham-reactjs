import React from 'react';
import Header from './header/Header.jsx'
import SkiDayCount from './SkiDayCount.jsx';
import SkiDayList from './SkiDayList.jsx';
import AddDay from './AddDay.jsx';
import AddDate from './AddDate.jsx';
import Menu from './Menu.jsx';
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      allSkiDays: [
        {
          resort: 'Vung Tau',
          date: new Date('1/1/2017'),
          powder: true,
          backcountry: false
        },
        {
          resort: 'Da Nang',
          date: new Date('1/1/2017'),
          powder: false,
          backcountry: false
        },
        {
          resort: 'Nha Trang',
          date: new Date('1/1/2017'),
          powder: false,
          backcountry: true
        },
      ]
    }
    this.addDay = this.addDay.bind(this);
  }
  addDay(newDate) {
    this.setState({
      allSkiDays: [... this.state.allSkiDays,
        newDate]
    })
  }
  render() {
    console.log(this.props.match.params.filter);
    return (
      <div className="container">
        <Menu />
        {(this.props.location.pathname === '/') ?
          <SkiDayCount total={50}
            powder={20}
            backcountry={10}
            goal={100}
          />
          :
          (this.props.location.pathname === '/add-day') ?
            <AddDate onNewDay={this.addDay} /> :
            <SkiDayList days={
              this.state.allSkiDays
            }
              filter={this.props.match.params.filter}
            />
        }
      </div>
    );
  }
}

export default App;