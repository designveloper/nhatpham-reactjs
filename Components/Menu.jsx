import React from 'react';
import { NavLink } from 'react-router-dom';
import  HomeIcon from 'react-icons/lib/fa/home';
import  AddDayIcon from 'react-icons/lib/fa/calendar-plus-o';
import  ListDaysIcon from 'react-icons/lib/fa/table';

const Menu = () => (
  <nav className="menu">
    <NavLink to="/" activeClassName="active">Home <HomeIcon/></NavLink>
    <NavLink to="/add-day" activeClassName="active">Add Day <AddDayIcon/></NavLink>
    <NavLink to="/list-days" activeClassName="active">List Days <ListDaysIcon/></NavLink>
  </nav>

)
export default Menu;