import React from 'react';
import ReactDOM from 'react-dom';
import App from './Components/App.jsx';
import NotFound from './Components/NotFound.jsx';
import AddDay from './Components/AddDay.jsx';
import SkiDayList from './Components/SkiDayList.jsx';

import { Router,Route, Switch } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';

ReactDOM.render(
  <Router history={createBrowserHistory()}>
    <Switch>
      <Route exact path='/' component={App} />
      <Route path='/add-day' component={App} />
      <Route path='/list-days/:filter' component={App}/>
      <Route path='/list-days' component={App}/>
      <Route path='*' component={NotFound} />
    </Switch>
  </Router>
  , document.getElementById('app'));